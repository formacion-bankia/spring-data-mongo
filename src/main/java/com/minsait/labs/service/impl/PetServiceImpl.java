package com.minsait.labs.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.minsait.labs.model.Category;
import com.minsait.labs.model.Pet;
import com.minsait.labs.persistence.entities.CategoryEntity;
import com.minsait.labs.persistence.entities.PetEntity;
import com.minsait.labs.persistence.repositories.CategoryRepository;
import com.minsait.labs.persistence.repositories.PetRepository;
import com.minsait.labs.service.PetService;

@Service
public class PetServiceImpl implements PetService {
	
	private PetRepository petRepository;
	private CategoryRepository categoryRepository;
	
	@Autowired
	public PetServiceImpl(PetRepository petRepository, CategoryRepository categoryRepository) {
		super();
		this.petRepository = petRepository;
		this.categoryRepository = categoryRepository;
	}

	@Override
	public void addPet(Pet pet) {
		PetEntity entity = new PetEntity();
		entity.setName(pet.getName());
		if(pet.getStatus()!=null){
			entity.setStatus(pet.getStatus().toString());
		}
		if(pet.getCategory()!=null){
			entity.setCategory(new CategoryEntity());
			entity.getCategory().setId(pet.getCategory().getId());
		}
		petRepository.save(entity);
	}

	@Override
	public void deletePet(Long petId) {
		petRepository.deleteById(petId);

	}

	@Override
	public Pet getPetById(Long petId) {
		Optional<PetEntity> optionalEntity = petRepository.findById(petId);
		Pet pet = null;
		if(optionalEntity.isPresent()){
			PetEntity entity = optionalEntity.get();
			pet = new Pet();
			pet.setId(entity.getId());
			pet.setName(entity.getName());
			pet.setStatus(Pet.StatusEnum.fromValue(entity.getStatus()));
			pet.setCategory(new Category());
			if(entity.getCategory()!=null){
				pet.getCategory().setId(entity.getCategory().getId());
				pet.getCategory().setName(entity.getCategory().getName());
			}
		}
		return pet;
	}

	@Override
	public void updatePet(Pet pet) {
		Optional<PetEntity> optionalEntity = petRepository.findById(pet.getId());
		if(optionalEntity.isPresent()){
			PetEntity entity = optionalEntity.get();
			entity.setName(pet.getName());
			if(pet.getStatus()!=null){
				entity.setStatus(pet.getStatus().toString());
			}
			if(pet.getCategory()!=null && pet.getCategory().getId()!=null){
				Optional<CategoryEntity> optionalCategory = categoryRepository.findById(pet.getCategory().getId());
				if(optionalCategory.isPresent()){
					entity.setCategory(optionalCategory.get());
				}	
			}
			petRepository.save(entity);
		}
		

	}

}
