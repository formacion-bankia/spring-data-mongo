package com.minsait.labs.service;

import com.minsait.labs.model.Pet;

public interface PetService {
	
	public void addPet(Pet pet);
	
	public void deletePet(Long petId);
	
	public Pet getPetById(Long petId);
	
	public void updatePet(Pet pet);

}
