package com.minsait.labs.api;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.minsait.labs.model.ModelApiResponse;
import com.minsait.labs.model.Pet;
import com.minsait.labs.service.PetService;

import io.swagger.annotations.ApiParam;

@Controller
public class PetApiController implements PetApi {
	
	private PetService petService;
	
	@Autowired
	public PetApiController(PetService petService) {
		super();
		this.petService = petService;
	}

	@Override
    @RequestMapping(value = "/pet",
            consumes = { "application/json", "application/xml" },
            method = RequestMethod.POST)
    public ResponseEntity<Void> addPet(@ApiParam(value = "Pet object that needs to be added to the store" ,required=true )  @Valid @RequestBody Pet body
    ) {
		petService.addPet(body);
		return new ResponseEntity<>(HttpStatus.CREATED);
	}

	@Override
    @RequestMapping(value = "/pet/{petId}",
    method = RequestMethod.DELETE)
	public ResponseEntity<Void> deletePet(@ApiParam(value = "Pet id to delete",required=true) @PathVariable("petId") Long petId
			,@ApiParam(value = "" ) @RequestHeader(value="api_key", required=false) String apiKey
			) {
		petService.deletePet(petId);
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}

	@Override
	public ResponseEntity<List<Pet>> findPetsByStatus(@NotNull @Valid List<String> status) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<List<Pet>> findPetsByTags(@NotNull @Valid List<String> tags) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
    @RequestMapping(value = "/pet/{petId}",
    produces = { "application/json", "application/xml" }, 
    method = RequestMethod.GET)
	public ResponseEntity<Pet> getPetById(@ApiParam(value = "ID of pet to return",required=true) @PathVariable("petId") Long petId
			){
		Pet pet = petService.getPetById(petId);
		if(pet!=null){
			return new ResponseEntity<Pet>(pet, HttpStatus.OK);
		}else{
			return new ResponseEntity<Pet>(HttpStatus.NOT_FOUND);
		}
	}

	@Override
    @RequestMapping(value = "/pet",
    consumes = { "application/json", "application/xml" },
    method = RequestMethod.PUT)
	public ResponseEntity<Void> updatePet(@ApiParam(value = "Pet object that needs to be added to the store" ,required=true )  @Valid @RequestBody Pet body
	){
		petService.updatePet(body);
		return new ResponseEntity<>(HttpStatus.CREATED);
	}

	@Override
	public ResponseEntity<Void> updatePetWithForm(Long petId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<ModelApiResponse> uploadFile(Long petId) {
		// TODO Auto-generated method stub
		return null;
	}

}
