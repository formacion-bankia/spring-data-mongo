package com.minsait.labs.persistence.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.minsait.labs.persistence.entities.CategoryEntity;

public interface CategoryRepository extends JpaRepository<CategoryEntity, Long> {

}
