package com.minsait.labs.persistence.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.minsait.labs.persistence.entities.PetEntity;

public interface PetRepository extends JpaRepository<PetEntity, Long> {

}
